# Quote of the Moment

![CircleCI](https://circleci.com/bb/minebreaker_tf/qotm.svg?style=svg&circle-token=da89db93cd79a403d3d8ac5e5a42c28485b3ea5a)

I created this project to learn Clojure & Compojure  
ClojureとCompojureの勉強  

## Build
```bat
@rem Google Sign-Inに使うクライアントIDをセットしたい場合
set $CLIENTID=YOUR-GOOGLE-API-CLIENT-ID
npm run build
lein uberjar
```
