'use strict';

var assert = chai.assert;

describe('SearchCtrl', function () {

    var $httpBackend, requestHandler, $componentController;

    beforeEach(function () {
        module('App');
    });
    beforeEach(inject(function (_$httpBackend_, _$componentController_) {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend
            .when('GET', /\/api\/1\/quotes\/?.*/)
            .respond({
                quotes: [
                    {"id": 1, "content": "Quote1", "author": "Author1", "source": "Source1"},
                    {"id": 2, "content": "Quote2", "author": "Author2", "source": "Source2"}
                ]
            });
        $componentController = _$componentController_;
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('SearchCtrl', function () {
        it('should send GET request when the search button is pushed.', function () {
            $httpBackend.expectGET(/\/api\/1\/quotes\/?.*/);
            var ctrl = $componentController('search', null, {
                q: {
                    content: "Quote",
                    author: "Author",
                    source: "Source"
                }
            });
            ctrl.submit();
            $httpBackend.flush();

            assert.equal(ctrl.quotes.length, 2);
            assert.equal(ctrl.quotes[0].content, 'Quote1');
            assert.equal(ctrl.quotes[0].author, 'Author1');
            assert.equal(ctrl.quotes[0].source, 'Source1');
            assert.equal(ctrl.quotes[0].id, '1');
            assert.equal(ctrl.quotes[1].content, 'Quote2');
            assert.equal(ctrl.quotes[1].author, 'Author2');
            assert.equal(ctrl.quotes[1].source, 'Source2');
            assert.equal(ctrl.quotes[1].id, '2');
            assert.isNull(ctrl.error);
        });

        it('should show error message when request failed.', function () {
            $httpBackend.expectGET(/\/api\/1\/quotes\/?.*/);
            requestHandler.respond(500, '');
            var ctrl = $componentController('search', null);
            ctrl.submit();
            $httpBackend.flush();

            assert.isNull(ctrl.quotes);
            assert.equal(ctrl.error, 'データの取得に失敗しました: 500 ');
        });

        it('should show message when there is no data.', function () {
            $httpBackend.expectGET(/\/api\/1\/quotes\/?.*/);
            requestHandler.respond({quotes: []});
            var ctrl = $componentController('search', null);
            ctrl.submit();
            $httpBackend.flush();

            assert.equal(ctrl.info, '該当するデータが存在しません');
            assert.isNull(ctrl.quotes);
            assert.isNull(ctrl.error);
        });
    });

});
