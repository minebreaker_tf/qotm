'use strict';

var assert = chai.assert;

describe('UserQuotesCtrl', function () {

    var $httpBackend, requestHandler, $componentController, $rootScope;

    beforeEach(function () {
        module('App');
    });
    beforeEach(inject(function (_$httpBackend_, _$componentController_, _$rootScope_) {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend
            .when('GET', '/api/1/users/123/quotes')
            .respond({
                quotes: [
                    {"id": 1, "content": "Quote1", "author": "Author1", "source": "Source1"},
                    {"id": 2, "content": "Quote2", "author": "Author2", "source": "Source2"}
                ]
            });
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('UserQuotesCtrl', function () {
        it('should show message when not logged in.', function () {
            var ctrl = $componentController('userQuotes', null);
            ctrl.$onInit();

            assert.equal(ctrl.info, 'ログインしていません');
        });

        it('should show quotes when instantiated.', function () {
            var ctrl = $componentController('userQuotes', null);
            $rootScope.user = {id: 123};
            ctrl.$onInit();
            $httpBackend.flush();

            assert.equal(ctrl.quotes.length, 2);
            assert.equal(ctrl.quotes[0].content, 'Quote1');
            assert.equal(ctrl.quotes[0].author, 'Author1');
            assert.equal(ctrl.quotes[0].source, 'Source1');
            assert.equal(ctrl.quotes[0].id, '1');
            assert.equal(ctrl.quotes[1].content, 'Quote2');
            assert.equal(ctrl.quotes[1].author, 'Author2');
            assert.equal(ctrl.quotes[1].source, 'Source2');
            assert.equal(ctrl.quotes[1].id, '2');
        });
    });

    describe('DeleteButtonCtrl', function () {
        it('should invoke callback function when clicked.', function () {
            var called = false;
            var ctrl = $componentController('deleteButton', null, {
                callback: function () {
                    called = true;
                }
            });

            assert.equal(ctrl.confirm, false);
            ctrl.fireEvent();
            assert.equal(ctrl.confirm, false);
            called = true;
        });
    });

});
