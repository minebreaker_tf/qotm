'use strict';

var assert = chai.assert;

describe('RandomCtrl', function () {

    var $httpBackend, requestHandler, $componentController;

    beforeEach(function () {
        module('App');
    });
    beforeEach(inject(function (_$httpBackend_, _$componentController_) {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend
            .when('GET', '/api/1/quotes?random=true')
            .respond({"id": 1, "content": "Quote", "author": "Author", "source": "Source"});
        $componentController = _$componentController_;
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('RandomCtrl', function () {
        it('should send GET request when instantiated.', function () {
            $httpBackend.expectGET('/api/1/quotes?random=true');
            var ctrl = $componentController('random', null);
            ctrl.$onInit();
            $httpBackend.flush();

            assert.equal(ctrl.quote.content, 'Quote');
            assert.equal(ctrl.quote.author, 'Author');
            assert.equal(ctrl.quote.source, 'Source');
            assert.equal(ctrl.quote.id, '1');
            assert.isUndefined(ctrl.error);
        });

        it('should show error message when request failed.', function () {
            requestHandler.respond(500, '');
            var ctrl = $componentController('random', null);
            ctrl.$onInit();
            $httpBackend.flush();

            assert.isUndefined(ctrl.quote);
            assert.equal(ctrl.error, 'データの取得に失敗しました: 500 ');
        });
    });

});
