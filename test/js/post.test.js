'use strict';

var assert = chai.assert;

describe('PostCtrl', function () {

    var $httpBackend, requestHandler, $componentController, $rootScope;

    beforeEach(function () {
        module('App');
    });
    beforeEach(inject(function (_$httpBackend_, _$componentController_, _$rootScope_) {
        $httpBackend = _$httpBackend_;
        requestHandler = $httpBackend
            .when('POST', '/api/1/quotes')
            .respond({status: 'success'});
        $componentController = _$componentController_;
        $rootScope = _$rootScope_;
    }));

    afterEach(function () {
        $httpBackend.verifyNoOutstandingExpectation();
        $httpBackend.verifyNoOutstandingRequest();
    });

    describe('PostCtrl', function () {

        it('should show message when posting was successful.', function () {
            $httpBackend.expectPOST('/api/1/quotes', {
                quote: {
                    content: "Quote",
                    author: "Author",
                    source: "Source"
                },
                id: 123
            }, function (header) {
                return header['X-CSRF-Token'] === 'ANTI-CSRF-TOKEN';
            });
            $rootScope.user = {id: 123};
            var ctrl = $componentController('post', null);
            $rootScope.csrfToken = 'ANTI-CSRF-TOKEN';

            assert.isTrue(ctrl.enable);
            ctrl.post({
                content: "Quote",
                author: "Author",
                source: "Source"
            });
            assert.isFalse(ctrl.enable);
            $httpBackend.flush();

            assert.equal(ctrl.info, 'データを追加しました');
            assert.equal(ctrl.error, '');
            assert.isTrue(ctrl.enable);
        });

        it('should show error message when request failed.', function () {
            $httpBackend.expectPOST('/api/1/quotes');
            $rootScope.user = {id: 123};
            requestHandler.respond(500, '');
            var ctrl = $componentController('post');
            ctrl.post();
            assert.isFalse(ctrl.enable);
            $httpBackend.flush();

            assert.equal(ctrl.error, 'データの追加に失敗しました: 500 ');
            assert.isTrue(ctrl.enable);
        });

        it('should stop to post when not logged in.', function () {
            $rootScope.idToken = null;
            var ctrl = $componentController('post');
            ctrl.post();

            assert.equal(ctrl.error, '投稿にはログインが必要です');
        });

    });

});
