(ns qotm.handler-test
  (:require [clojure.test :refer :all]
            [ring.mock.request :as mock]
            [qotm.handler :refer :all]))

(deftest test-handler
  (testing "routing"

    (let [response (app (mock/request :get "/"))]
      (is (= (:status response) 302)))

    (let [response (app (mock/request :get "/api/1/csrf"))]
      (is (= (:status response) 200)))
    
    (let [response (app (mock/request :get "/some/url/that/does/not/exists"))]
      (is (= (:status response) 404)))    
      
      ))
