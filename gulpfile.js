const gulp = require('gulp');
//noinspection NpmUsedModulesInstalled
const ts = require('gulp-typescript');
const browserify = require('browserify');
const source = require('vinyl-source-stream');
const rimraf = require('rimraf');
const replace = require('gulp-replace');

gulp.task('clean', function (callback) {
    return rimraf('./resources/public/*', callback);
});

gulp.task('setClientId', function () {
    var id = process.env.$CLIENTID;
    console.log('ClientID: ' + id);
    if (typeof id === 'undefined') {
        console.log('ClientID is not specified. Make sure this is not the production build.');
        id = 'invalid';
    }
    return gulp.src('src/js/index.html')
        .pipe(replace('${CLIENT-ID}', id))
        .pipe(gulp.dest('resources/public/'));
});

gulp.task('compile', ['setClientId'], function () {
    const tsProject = ts.createProject('tsconfig.json');

    //noinspection JSUnresolvedFunction
    return tsProject.src()
        .pipe(tsProject())
        .on("error", function () {
            this.failed = true;
        })
        .on("finish", function () {
            this.failed && process.exit(1);
        })
        .js
        .pipe(gulp.dest('target/js/'));
});

gulp.task('browserify', ['compile'], function () {
    //noinspection JSUnresolvedFunction
    return browserify({
        basedir: '.',
        debug: true,
        entries: [
            './target/js/index.js'
        ],
        noParse: [],
        cache: {},
        packageCache: {}
    })
        .bundle()
        .pipe(source('index.js'))
        .pipe(gulp.dest('resources/public/'));
});

gulp.task('build', ['setClientId', 'compile', 'browserify']);

gulp.task('default', ['build']);
