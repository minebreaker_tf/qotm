declare const $: any;

const userQuotes: ng.IComponentOptions = {
    template: `
    <div class="container">
        <h2>Your quotes</h2>
    </div>
    <div class="container">
        <info message="$ctrl.info"></info>
        <warnings message="$ctrl.error"></warnings>
        <div class="container-fluid" ng-repeat="quote in $ctrl.quotes">
            <quote quote="quote"></quote>
            <delete-button callback="$ctrl.deleteQuote(quote.id)"></delete-button>
            <update-button modal-id="'modal' + quote.id" quote="quote" callback="$ctrl.updateQuote(quote)"></update-button>
        </div>
    </div>
    <div class="container">
        <a ng-href="#!/main">Top</a><br />
        <a ng-href="#!/search">Search quotes</a><br />
        <a ng-href="#!/random">Random quote</a><br />
        <a ng-href="#!/post">Post quotes</a>
    </div>`,
    controller: class {

        quotes: any[];
        info: string;
        error: string;

        constructor(private $http: ng.IHttpService, private $rootScope: any) {}

        $onInit() {
            if (typeof this.$rootScope.user === 'undefined' || typeof this.$rootScope.user.id !== 'number') {
                this.info = 'ログインしていません';
                return;
            }
            this.$http({
                method: 'GET',
                url: `/api/1/users/${this.$rootScope.user.id}/quotes`
            }).then((response: ng.IHttpPromiseCallbackArg<any>) => {
                this.quotes = response.data.quotes;
            }, (response: ng.IHttpPromiseCallbackArg<any>) => {
                this.error = 'データの取得に失敗しました: ' + response.status + ' ' + response.statusText;
            });
        }

        updateQuote(quote: any) {
            this.$http({
                method: 'PUT',
                url: `/api/1/quotes/${quote.id}`,
                params: { user: this.$rootScope.user.id },
                headers: { 'X-CSRF-Token': this.$rootScope.csrfToken },
                data: { quote: quote }
            }).then(() => {
                this.info = 'データを更新しました';
            }, (response: ng.IHttpPromiseCallbackArg<any>) => {
                this.error = 'データの更新に失敗しました: ' + response.status + ' ' + response.statusText;
            });
        }

        deleteQuote(id: number) {
            this.$http({
                method: 'DELETE',
                url: `/api/1/quotes/${id}`,
                params: { user: this.$rootScope.user.id },
                headers: { 'X-CSRF-Token': this.$rootScope.csrfToken }
            }).then(() => {
                this.info = 'データを削除しました';
                this.quotes = this.quotes.filter(e => e.id !== id);
            }, (response: ng.IHttpPromiseCallbackArg<any>) => {
                this.error = 'データの削除に失敗しました: ' + response.status + ' ' + response.statusText;
            });
        }

    }
};

export const updateButton: ng.IComponentOptions = {
    bindings: {
        modalId: '<',
        callback: "&",
        quote: '<'
    },
    template: `
    <div>
        <button class="btn btn-default pull-right" ng-click="$ctrl.show()">Update</button>
        <div class="modal fade" ng-attr-id="{{ $ctrl.modalId }}">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button class="close" ng-click="$ctrl.hide()"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update</h4>
                    </div>
                    <div class="modal-body">
                        <quote-form button-text="'Update'" button-enable="true" callback="$ctrl.submit()" quote="$ctrl.quote"></quote-form>
                        <button class="btn btn-default" ng-click="$ctrl.hide()">Dismiss</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `,
    controller: class {

        modalId: string;
        quote: any;
        callback: Function;

        show() {
            $('#' + this.modalId).modal('show');
        }

        hide() {
            $('#' + this.modalId).modal('hide');
        }

        submit() {
            this.callback({ quote: this.quote });
            this.hide();
        }
    }
};

export const deleteButton: ng.IComponentOptions = {
    bindings: {
        callback: "&",
    },
    template: `
    <div class="container-fluid pull-right">
        <div ng-if="$ctrl.confirm">
            Are you sure?
            <button type="submit" class="btn btn-default" ng-click="$ctrl.fireEvent()">Yes</button>
            <button class="btn btn-default" ng-click="$ctrl.confirm = false">No</button>
        </div>
        <button ng-if="!$ctrl.confirm" class="btn btn-default" ng-click="$ctrl.confirm = true">Delete</button>
    </div>`,
    controller: class {
        confirm: boolean = false;
        callback: Function;

        fireEvent() {
            this.callback();
            this.confirm = false;
        }
    }
};

export default userQuotes;
