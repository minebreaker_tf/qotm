declare const gapi: any;

const login: ng.IComponentOptions = {
    template: `
    <div class="container"><div class="pull-right">
        <table><tr>
            <td> User: {{ $ctrl.mail }} </td>
            <td><div id="login-btn"></div></td>
            <td><button class="btn btn-default" ng-click="$ctrl.logout();">Logout</button></td>
        </tr></table>
    </div></div>`,
    controller: class {
        mail: string;

        constructor(private $scope: ng.IScope, private $rootScope: any, private $http: ng.IHttpService) { }

        $onInit() {
            //noinspection SpellCheckingInspection
            gapi.signin2.render('login-btn', {
                'scope': 'profile email',
                'width': 140,
                'height': 30,
                'longtitle': false,
                'theme': 'dark',
                'onsuccess': this.login,
                'onfailure': () => {
                    console.log('err!');
                }
            });
        };

        logout() {
            gapi.auth2.getAuthInstance().signOut().then(() => {
                console.log('User signed out.');
                this.mail = '';
                this.$scope.$apply();
                this.$rootScope.user.id = '';
            });
        };

        //noinspection SpellCheckingInspection
        login = (googleUser: any) => {
            const profile = googleUser.getBasicProfile();
            console.log(profile);
            console.log(profile.getEmail());
            console.log(googleUser.getAuthResponse().id_token);
            this.mail = profile.getEmail();
            this.$scope.$apply(); // 求: $scopeを使わないやり方
            this.queryUserInfo(googleUser.getAuthResponse().id_token);
            this.queryCsrfToken();
        };

        private queryUserInfo(token: string) {
            this.$http({
                method: 'GET',
                url: '/api/1/login',
                params: { token: token }
            }).then((response: ng.IHttpPromiseCallbackArg<any>) => {
                // AngularはServiceで変数を共有することを推奨していない
                // http://stackoverflow.com/questions/18880737/how-do-i-use-rootscope-in-angular-to-store-variables
                this.$rootScope.user = { id: response.data.id };
            }, (response: ng.IHttpPromiseCallbackArg<any>) => {
                console.log('err!');
                console.log(response);
            });
        }

        private queryCsrfToken() {
            this.$http({
                method: 'GET',
                url: '/api/1/csrf'
            }).then((response: ng.IHttpPromiseCallbackArg<any>) => {
                this.$rootScope.csrfToken = response.data;
            }, (response: ng.IHttpPromiseCallbackArg<any>) => {
                console.log('err!');
                console.log(response);
            });
        }

    }
};

export default login;
