import angular = require('angular');
import angularRoute = require('angular-route');

import search from './search';
import random from './random';
import post from './post';
import login from './login';
import userQuotes, {deleteButton, updateButton} from './userQuotes';
import * as common from './common';

// TODO 型
// TODO strictNullCheck

const app: ng.IModule = angular.module('App', [angularRoute]);

app.config(['$routeProvider', ($routeProvider: ng.route.IRouteProvider) => {
    $routeProvider
        .when('/', { template: '<main></main>' })
        .when('/search', { template: '<search></search>' })
        .when('/random', { template: '<random></random>' })
        .when('/post', { template: '<post></post>' })
        .when('/user/quotes', { template: '<user-quotes></user-quotes>' })
        .otherwise({ redirectTo: '/' });
}]);

app.component('main', {
    template: `
    <div class="container">
        <h1>Quote of the Moment</h1>
        <h3><a ng-href="#!/search">Search quotes</a></h3>
        <h3><a ng-href="#!/random">Random quote</a></h3>
        <h3><a ng-href="#!/post">Post quotes</a></h3>
        <h3><a ng-href="#!/user/quotes">Your quotes</a></h3>
    </div>`
});

app.component('search', search);
app.component('random', random);
app.component('post', post);
app.component('login', login);
app.component('userQuotes', userQuotes);
app.component('deleteButton', deleteButton);
app.component('updateButton', updateButton);

app.component('quoteForm', common.quoteForm);
app.component('quote', common.quote);
app.component('info', common.info);
app.component('warning', common.warning);
