import IHttpProvider = angular.IHttpProvider;

const post: ng.IComponentOptions = {
    template: `
    <div class="container">
        <h2>Post</h2>
        <quote-form button-text="'Post'" button-enable="$ctrl.enable" callback="$ctrl.post(quote)"></quote-form>
    </div>
    <div class="container">
        <info message="$ctrl.info"></info>
        <warnings message="$ctrl.error"></warnings>
    </div>
    <div class="container">
        <a ng-href="#!/main">Top</a><br />
        <a ng-href="#!/search">Search quotes</a><br />
        <a ng-href="#!/random">Random quote</a><br />
        <a ng-href="#!/user/quotes">Your quotes</a><br />
    </div>`,
    controller: class {
        info: string;
        error: string;
        enable: boolean = true;

        constructor(private $http: ng.IHttpService, private $rootScope: any) {}

        post(quote: any) {
            this.enable = false;
            this.info = '';
            this.error = '';

            // IDトークンを設定
            if (typeof this.$rootScope.user === 'undefined' || typeof this.$rootScope.user.id !== 'number') {
                this.error = '投稿にはログインが必要です';
                this.enable = true;
                return;
            }

            this.$http({
                method: 'POST',
                url: '/api/1/quotes',
                data: {
                    quote: quote,
                    id: this.$rootScope.user.id
                },
                headers: { 'X-CSRF-Token': this.$rootScope.csrfToken }
            }).then(() => {
                this.info = 'データを追加しました';
                this.enable = true;
            }, (response: ng.IHttpPromiseCallbackArg<any>) => {
                this.error = 'データの追加に失敗しました: ' + response.status + ' ' + response.statusText;
                this.enable = true;
            });
        };
    }
};

export default post;
