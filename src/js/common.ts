export const quoteForm: ng.IComponentOptions = {
    bindings: {
        callback: '&',
        buttonEnable: '=',
        buttonText: '<',
        quote: '<'
    },
    template: `
        <div class="form-group">
            <label for="content">Quote:</label>
            <input id="content" class="form-control" type="text" ng-model="$ctrl.quote.content"/>
            <label for="author">Author:</label>
            <input id="author" class="form-control" type="text" ng-model="$ctrl.quote.author"/>
            <label for="source">Source:</label>
            <input id="source" class="form-control" type="text" ng-model="$ctrl.quote.source"/>
            <button class="btn btn-default" ng-click="$ctrl.invokeCallback()" ng-disabled="!$ctrl.buttonEnable">
                {{ $ctrl.buttonText }}
            </button>
        </div>`,
    controller: class {
        quote: any;
        callback: Function;

        invokeCallback() {
            this.callback({ quote: this.quote });
        }
    }
};

export const quote: ng.IComponentOptions = {
    bindings: {
        quote: '<'
    },
    template: `
    <div>
        <blockquote>
            <p>{{ $ctrl.quote.content }}</p>
            <footer>
                {{ $ctrl.quote.author }}
                <cite title="$ctrl.quote.source">{{ $ctrl.quote.source }}</cite>
            </footer>
        </blockquote>
    </div>`
};

export const info: ng.IComponentOptions = {
    bindings: {
        message: '<'
    },
    template: `
    <div class="alert alert-info alert-dismissible bg-info" ng-if=$ctrl.message>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <span class="glyphicon glyphicon glyphicon-info-sign" aria-hidden="true"></span>
        {{ $ctrl.message }}
    </div>`
};

export const warning: ng.IComponentOptions = {
    bindings: {
        message: '<'
    },
    template: `
    <div class="alert alert-danger bg-danger" ng-if=$ctrl.message>
        <span class="glyphicon glyphicon glyphicon-alert" aria-hidden="true"></span>
        {{ $ctrl.message }}
    </div>`
};
