const search: ng.IComponentOptions = {
    template: `
    <div class="container">
        <h2>Search</h2>
        <quote-form button-text="'Search'" button-enable="true" callback="$ctrl.submit(quote)"></quote-form>
    </div>
    <div class="container">
        <info message="$ctrl.info"></info>
        <warnings message="$ctrl.error"></warnings>
        <quote ng-repeat="quote in $ctrl.quotes" quote="quote"></quote>
    </div>
    <div class="container">
        <a ng-href="#!/main">Top</a><br />
        <a ng-href="#!/random">Random quote</a><br />
        <a ng-href="#!/post">Post quotes</a><br />
        <a ng-href="#!/user/quotes">Your quotes</a><br />
    </div>`,
    controller: class {
        info: string;
        error: string;
        quotes: any[];

        constructor(private $http: ng.IHttpService) {}

        submit(quote: any) {
            this.quotes = null;
            this.error = null;

            this.$http({
                method: 'GET',
                url: '/api/1/quotes',
                params: { query: quote }
            }).then((response: ng.IHttpPromiseCallbackArg<any>) => {
                if (response.data.quotes.length === 0) {
                    this.info = '該当するデータが存在しません';
                    return;
                }
                this.quotes = response.data.quotes;
            }, (response: ng.IHttpPromiseCallbackArg<any>) => {
                this.error = 'データの取得に失敗しました: ' + response.status + ' ' + response.statusText;
            });
        }
    }
};

export default search;
