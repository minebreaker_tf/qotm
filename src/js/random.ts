const random: ng.IComponentOptions = {
    template: `
    <div class="container">
        <h2>Random quote</h2>
        <warnings message="$ctrl.error"></warnings>
        <quote ng-if="$ctrl.quote.content" quote="$ctrl.quote"></quote>
        <!-- TODO refresh -->
    </div>
    <div class="container">
        <a ng-href="#!/main">Top</a><br />
        <a ng-href="#!/search">Search quotes</a><br />
        <a ng-href="#!/post">Post quotes</a><br />
        <a ng-href="#!/user/quotes">Your quotes</a><br />
    </div>`,
    controller: class {
        quote: string;
        error: string;

        constructor(private $http: ng.IHttpService) {}

        $onInit() {
            this.$http({
                method: 'GET',
                url: '/api/1/quotes',
                params: { random: true }
            }).then((response: ng.IHttpPromiseCallbackArg<any>) => {
                this.quote = response.data;
            }, (response: ng.IHttpPromiseCallbackArg<any>) => {
                this.error = 'データの取得に失敗しました: ' + response.status + ' ' + response.statusText;
            });
        }
    }
};

export default random;
