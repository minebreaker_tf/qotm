(ns qotm.gauth
  (:require [cheshire.core :refer :all])
  (:import (java.net InetSocketAddress Proxy)
           (com.google.api.client.googleapis.auth.oauth2 GoogleIdToken GoogleIdTokenVerifier)
           (com.google.api.client.googleapis.util Utils)
           (com.google.api.client.http.javanet NetHttpTransport NetHttpTransport$Builder)))

(def proxy-to-use Proxy/NO_PROXY)

(def httpTransport
  (-> (new NetHttpTransport$Builder)
    (. setProxy proxy-to-use)
    (. build)))

(defn get-payload [token]
  (-> (new GoogleIdTokenVerifier httpTransport (Utils/getDefaultJsonFactory))
    (. verify token)
    (. getPayload)))

(defn get-gauth-info [token]
  (if-let [payload (get-payload token)]
    {:token token
      :id    (. payload getSubject)
      :email (. payload getEmail)
      :name  (. payload get "name")}))
