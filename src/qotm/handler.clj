(ns qotm.handler
  (:require [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.defaults :refer [wrap-defaults site-defaults]]
            [ring.middleware.session :refer [wrap-session]]
            [ring.middleware.anti-forgery :refer :all]
            [ring.util.response :as resp]
            [cheshire.core :refer :all]
            [qotm.query :refer :all]
            [qotm.gauth :refer :all]))

(defn response [body & [code]] {:status (if (nil? code) 200 code) :headers {"Content-Type" "text/json"} :body body})

(def per-page 20)
(defn page-to-ind [param]
  (let [page (get param :page 0)]
    {:start (+ 1 (* page per-page))
     :end   (+ per-page (* page per-page))}))

(defn get-quotes [param]
  (let [ind (page-to-ind param)]
    (generate-string
      (if (param :random)
        (first (query-quote-rand))
        {:quotes (query-quote-all (ind :start) (ind :end) (parse-string (param :query) true))
         :page   (get param :page 0)}))))

(defn get-body [body]
  (parse-string (slurp body) true))

(defn post-quote [body-str]
  (let [body (get-body body-str)]
    (insert-quote!
      (merge
        (body :quote)
        {:creator_id (body :id)}))
    (response (generate-string {:status "Success"}))))

(defn get-quote-of-user [id]
  (generate-string {:quotes (query-quotes-of-user id)}))

(defn put-quotes [body-str param]
  (let [body (get-body body-str)]
    (update-quote!
      (param :id)
      (body :quote)
      (param :user))
    (response (generate-string {:status "Success"}))))

(defroutes app-routes
  (GET "/" [] (resp/redirect "index.html"))
  (GET "/api/1/quotes" {param :params} (get-quotes param))
  (GET "/api/1/quotes/:id" [id] (generate-string (query-quote id)))
  (POST "/api/1/quotes" {body :body} (post-quote body))
  (PUT "/api/1/quotes/:id" {body :body param :params} (put-quotes body param))
  (DELETE "/api/1/quotes/:id" [id user] (delete-quote! id user))
  (GET "/api/1/users/:id/quotes" [id] (get-quote-of-user id))
  (GET "/api/1/login" [token] (generate-string {:id (get-user! (get-gauth-info token))}))
  (GET "/api/1/csrf" [] *anti-forgery-token*)
  (route/not-found "Not Found"))

(def app
  (-> app-routes
    (wrap-defaults site-defaults)
    (wrap-session {:cookie-attrs {:max-age 30}})))
; (def app (wrap-defaults app-routes (assoc-in site-defaults [:security :anti-forgery] false)))
