(ns qotm.query
  (:require [clojure.java.jdbc :as jdbc]))

(def db {:connection-uri "jdbc:h2:test"})

(defn init! []
  (do
    (jdbc/execute! db "DROP TABLE IF EXISTS quotes")
    (jdbc/execute! db "DROP TABLE IF EXISTS users")
    (jdbc/execute! db "CREATE TABLE quotes (
                          id INT PRIMARY KEY AUTO_INCREMENT,
                          content VARCHAR2 NOT NULL,
                          author VARCHAR2 NOT NULL,
                          source VARCHAR2 NOT NULL,
                          creator_id INT NOT NULL
                        )")
    (jdbc/execute! db "CREATE TABLE users (
                         id INT PRIMARY KEY AUTO_INCREMENT,
                         google_id VARCHAR2 UNIQUE NOT NULL,
                         name VARCHAR2 NOT NULL,
                         email VARCHAR2 NOT NULL
                       )")
    (jdbc/insert! db :quotes {:content "Quote1" :author "Author1" :source "Source1" :creator_id 1})
    (jdbc/insert! db :quotes {:content "Quote2" :author "Author2" :source "Source2" :creator_id 2})
    (jdbc/insert! db :quotes {:content "Quote3" :author "Author3" :source "Source3" :creator_id 3})
    (jdbc/insert! db :users {:google_id "123" :name "abc" :email "foo@foomail.com"})
    (jdbc/insert! db :users {:google_id "456" :name "def" :email "bar@barmail.com"})
    (jdbc/insert! db :users {:google_id "789" :name "ghi" :email "buz@buzmail.com"})))

(defn query-quote [id]
  (first (jdbc/query db ["SELECT id, content, author, source FROM quotes WHERE quotes.id = ?" id])))

(defn query-quote-all [st ed q]
  (let [query (if (nil? q) {:content "" :author "" :source ""} q)]
    (jdbc/query db ["SELECT
                       id, content, author, source
                     FROM
                       quotes
                     WHERE
                       ROWNUM between ? and ? AND
                       LOWER(quotes.content) LIKE LOWER(CONCAT('%', ?, '%')) AND
                       LOWER(quotes.author) LIKE LOWER(CONCAT('%', ?, '%')) AND
                       LOWER(quotes.source) LIKE LOWER(CONCAT('%', ?, '%'))"
                    st ed (query :content) (query :author) (query :source)])))

(defn query-quote-rand []
  (jdbc/query db ["SELECT id, content, author, source, RAND() as rand FROM quotes ORDER BY rand LIMIT 1"]))

(defn insert-quote! [quote]
  (jdbc/insert! db :quotes quote))

(defn update-quote! [quote-id q user-id]
  (if (or (nil? user-id) (nil? q)) (throw (IllegalArgumentException.)))
  (jdbc/update! db :quotes q ["id = ? AND creator_id = ?" quote-id user-id]))

(defn delete-quote! [id user-id]
  (if (or (nil? id) (nil? user-id)) (throw (IllegalArgumentException.)))
  (jdbc/delete! db :quotes ["id = ? AND creator_id = ?" id user-id]))

(defn query-user-by-google-id [google-id]
  (jdbc/query db ["SELECT id, google_id, email FROM users WHERE users.google_id = ?" google-id]))

(defn query-quotes-of-user [id]
  (jdbc/query db ["SELECT
                     quotes.id AS id,
                     quotes.content AS content,
                     quotes.author AS author,
                     quotes.source AS source
                   FROM
                     quotes JOIN users ON quotes.creator_id = users.id
                   WHERE
                     users.id = ?"
                  id]))

(defn insert-user! [gauth-info]
  (->
    (jdbc/insert! db :users {:google_id (gauth-info :id) :email (gauth-info :email) :name (gauth-info :name)})
    (first)
    (vals)
    (first)))

(defn get-user!
  "ユーザーが登録されていた場合、そのIDを返す。そうでない場合ユーザーを登録し、そのIDを返す。"
  [gauth-info]
  (let [user (query-user-by-google-id (gauth-info :id))]
    (if (= (count user) 0)
      (insert-user! gauth-info)
      ((first user) :id))))
