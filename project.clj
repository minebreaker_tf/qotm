(defproject qotm "0.1.0-SNAPSHOT"
  :description "qotm"
  :url "https://minebreaker_tf@bitbucket.org/minebreaker_tf/qotm.git"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/java.jdbc "0.7.0-alpha1"]
                 [compojure "1.5.1"]
                 [ring/ring-defaults "0.2.1"]
                 [com.h2database/h2 "1.3.176"]
                 [cheshire "5.6.3"]
                 [com.google.api-client/google-api-client "1.22.0"]]
  :plugins [[lein-ring "0.9.7"] [lein-test-out "0.3.1"]]
  :ring {:handler qotm.handler/app}
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
